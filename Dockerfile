FROM ubuntu:22.04

ARG CHARMCRAFT_VERSION
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

# Install reference : https://github.com/canonical/charmcraft#install 

RUN apt install python3 python3-pip libffi-dev libapt-pkg-dev libssl-dev python3-venv curl git -y
RUN pip3 install https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/python-apt/2.3.0ubuntu2/python-apt_2.3.0ubuntu2.tar.xz
RUN pip3 install charmcraft==$CHARMCRAFT_VERSION

RUN charmcraft version
RUN python3 --version
