# build-charms-docker

This project holds a Docker image which can be utilized to run `charmcraft`

## Getting started

If you want to see what Charmcraft is, just goto https://github.com/canonical/charmcraft

Otherwise, enjoy!
